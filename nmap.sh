#!/bin/bash

#     _____                _              _       
#    / ____|              | |            | |      
#   | |     ___  _ __  ___| |_ __ _ _ __ | |_ ___ 
#   | |    / _ \| '_ \/ __| __/ _` | '_ \| __/ __|
#   | |___| (_) | | | \__ \ || (_| | | | | |_\__ \
#    \_____\___/|_| |_|___/\__\__,_|_| |_|\__|___/

GREEN="\033[32;1m"
NORMAL="\033[0m"

#    ______                _   _                 
#   |  ____|              | | (_)                
#   | |__ _   _ _ __   ___| |_ _  ___  _ __  ___ 
#   |  __| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
#   | |  | |_| | | | | (__| |_| | (_) | | | \__ \
#   |_|   \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
#                                                
# ascii art: http://www.patorjk.com/software/taag/#p=display&c=bash&f=Big&t=Functions

function .print_system_info () {
    echo -e ">>>>>>>>>>>>>>>>>>>>${GREEN}Informação do Sistema:${NORMAL}<<<<<<<<<<<<<<<<<<<<\n"
    hostnamectl
    echo "-------------------------------------------------------------"
    echo -e "\n\tNumero de cores:\t"`grep -c 'processor' /proc/cpuinfo`
    echo -e "\tModelo do processador:\t"`cat /proc/cpuinfo | sed '5!d' | cut -d: -f2`
    echo -e "\n>>>>>>>>>>>>>>>>>>>>${GREEN}Fim da Informação do Sistema${NORMAL}<<<<<<<<<<<<<<<<<<<<\n"
}

function .validar_instalar_nmap () {
    echo "Escolheste opção 1"

    if ! command -v nmap >/dev/null 2>&1 ; then
        echo "Nmap está a ser instalado, aguarde..."  
        sudo apt install nmap -y
    else
        echo    "Instalado"
        exit 0 # 0 == success; otherwise == some error
    fi
}

function .listar_ips_activos () {
    echo "Escolheste opção 2"
    nmap -sn 192.168.1.0/24
}

function .listar_portos_abertos () {
    echo "Escolheste a opção 3"
    nmap --top-ports 20 192.168.1.0/24
}

#    __  __       _       
#   |  \/  |     (_)      
#   | \  / | __ _ _ _ __  
#   | |\/| |/ _` | | '_ \ 
#   | |  | | (_| | | | | |
#   |_|  |_|\__,_|_|_| |_|
#                         
# ascii art: http://www.patorjk.com/software/taag/#p=display&c=bash&f=Big&t=Main


.print_system_info


echo -e "${GREEN}Escolhe um dos numeros abaixo:${NORMAL}"
echo "-------------------------------------------------------------"

PS3='Escolhe uma opção: '
options=("Validar/Instalar Nmap" "Listar Endereços ips ativos" "Listar portos abertos" "Encerrar")
select opt in "${options[@]}"
do
    case $opt in
        "Validar/Instalar Nmap")
            .validar_instalar_nmap ;;
         "Listar Endereços ips ativos")
            .listar_ips_activos ;;
        "Listar portos abertos")
            .listar_portos_abertos ;;
        "Encerrar")
            echo "Programa terminado"
            break ;;
        *) echo "Opção Inválida $REPLY";;
    esac
done
